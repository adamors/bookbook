class OrderMailer < ApplicationMailer
  def new_order(order)
    @order = order
    mail(to: order.user.email, subject: 'Your newest order')
  end

  def new_customer_order(order)
    @order = order
    mail(to: 'admin@bookstore.com', subject: 'New customer order')
  end
end
