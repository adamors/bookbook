class Order < ApplicationRecord
  has_many :line_items
  belongs_to :country
  belongs_to :user, optional: true

  enum payment_type: {
    'Credit card' => 0,
    'Pay on delivery' => 1
  }
  enum status: {
    unconfirmed: 0,
    confirmed: 1
  }
  validates :name, :address, :country_id, :payment_type, presence: true
  validates :payment_type, inclusion: payment_types.keys

  AMOUNT_FOR_FREE_SHIPPING = 400

  def add_cart_contents(cart)
    cart.line_items.each do |item|
      #item.cart_id = nil
      line_items << item
    end
  end

  def update_book_sold_count
    line_items.each do |line_item|
      book = line_item.book
      book.update_attribute(:sold_count, book.sold_count + line_item.quantity)
    end
  end

  def total
    @total ||= line_items.to_a.sum { |item| item.price * item.quantity }
  end

  def item_count
    line_items.map(&:quantity).sum
  end

  def total_with_tax
    total + tax
  end
end
