class LineItem < ApplicationRecord
  belongs_to :cart, optional: true
  belongs_to :order, optional: true
  belongs_to :book

  validates :quantity, presence: true, numericality: { greater_than_or_equal_to: 0 }
end
