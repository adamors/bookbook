class Cart < ApplicationRecord
  has_many :line_items

  accepts_nested_attributes_for :line_items

  def add_book(book)
    line_item = line_items.find_by(book_id: book.id)
    if line_item
      line_item.quantity += 1
    else
      line_item = line_items.build(book_id: book.id)
      line_item.price = line_item.book.price
    end
    line_item
  end

  def empty?
    line_items.count.zero?
  end

  def count
    line_items.count
  end

  def total_line_items
    line_items.to_a.sum { |item| item.price * item.quantity }
  end

  alias total total_line_items
end
