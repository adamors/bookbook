class User < ApplicationRecord
  has_secure_password
  validates_presence_of :name, :password
  validates :email, uniqueness: true, presence: true
  has_many :orders
end
