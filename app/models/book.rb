class Book < ApplicationRecord
  belongs_to :category
  belongs_to :author
  has_many :line_items
  has_many :orders, through: :line_items

  def self.newest(limit)
    includes(:author).first(limit)
  end

  def self.best_selling(limit)
    includes(:author).order(sold_count: :desc).limit(limit)
  end
end
