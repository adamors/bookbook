module CartHelper
  def set_cart
    @cart = Cart.find_by_id session[:cart_id]
    return if @cart
    @cart = Cart.create
    session[:cart_id] = @cart.id
  end
end
