class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :categories
  include SessionsHelper
  include AdminHelper

  def categories
    @categories = Category.all
  end

  def authenticate
  end
end
