class AdminController < ApplicationController
  before_action :check_admin

  def orders
    @orders = Order.all
  end

  def users
    @users = User.all
  end

  private

  def check_admin
    return true if admin_logged_in?
    redirect_to store_index_path, notice: 'You are not allowed to access that page'
  end
end
