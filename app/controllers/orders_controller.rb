class OrdersController < ApplicationController
  include CartHelper
  before_action :check_cart, except: [:index, :show]
  before_action :check_login
  before_action :find_existing_order, only: [:checkout, :finish_checkout]

  def index
    @orders = Order.confirmed.where(user_id: @current_user.id)
  end

  def show
    id = params[:id]
    @order = Order.confirmed.where(user_id: @current_user.id).where(id: id).first
    unless @order
      redirect_to orders_path, alert: 'Order not found'
      return
    end
  end

  def address
    order.name ||= current_user.name
    @countries = Country.all
  end

  def save_address
    order_params = params.require(:order).permit(:name, :address, :country_id, :payment_type)
    order.assign_attributes(order_params)
    unless @order.save
      redirect_to add_order_address_path, alert: "Error saving order. #{@order.errors.full_messages.join(', ')}"
      return
    end
    @order.add_cart_contents(@cart)
    session[:order_id] = @order.id
    redirect_to orders_checkout_path
  end

  def checkout
    unless @order
      redirect_to store_index_path, error: 'Order error'
      return
    end
    @order.tax = @order.total * @order.country.tax / 100.0
    @order.shipping_cost = @order.total_with_tax > Order::AMOUNT_FOR_FREE_SHIPPING ? 0 : 10
    @order.save
  end

  def finish_checkout
    @order.confirmed!
    @order.update_attribute(:user_id, @current_user.id)
    @order.update_book_sold_count
    OrderMailer.new_order(@order).deliver_now
    OrderMailer.new_customer_order(@order).deliver_now
    session.delete :order_id
    session.delete :cart_id
    redirect_to root_path, notice: 'Order sent successfully'
  end

  private

  def order
    find_existing_order
    @order ||= Order.new
    @order
  end

  def find_existing_order
    @order = Order.find_by_id session[:order_id]
  end

  def check_cart
    set_cart
    if @cart.nil? || @cart.empty?
      redirect_to store_index_path, notice: 'Cart empty'
      return
    end
  end

  def check_login
    return true if logged_in?
    redirect_to new_session_path, notice: 'Log in to continue'
  end
end
