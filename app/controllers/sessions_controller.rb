class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: user_params[:email].downcase)
    if user
      if user.authenticate(user_params[:password])
        log_in user
        return redirect_to store_index_path, notice: 'Successfully logged in'
      end
    end
    flash[:danger] = 'Invalid email/password combination'
    render 'new'
  end

  def destroy
    session.delete :user_id
    redirect_to store_index_path, notice: 'Successfully logged out'
  end

  def user_params
    params.require(:session).permit(:email, :password)
  end
end
