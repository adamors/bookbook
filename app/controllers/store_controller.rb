class StoreController < ApplicationController
  def index
    @new_books = Book.newest(10)
    @best_books = Book.best_selling(10)
  end
end
