class CartController < ApplicationController
  include CartHelper
  before_action :set_cart

  def show
  end

  def update_quantity
    line_items = params.require(:cart).permit(line_items_attributes: [:id, :quantity])
    @cart.update line_items
    @cart.line_items.where(quantity: 0).destroy_all
    redirect_to view_cart_path, notice: 'Quantities updated'
  end

  def add_book
    book = Book.find params[:book_id]
    @line_item = @cart.add_book book
    if @line_item.save
      redirect_to view_cart_path, notice: 'Added book to cart'
    else
      redirect_to book_path(book), alert: 'Error adding book to cart. ' + @line_item.errors.full_messages.to_sentence
    end
  end
end
