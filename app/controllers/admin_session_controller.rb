class AdminSessionController < ApplicationController
  def new
  end

  def create
    admin = Admin.find_by(username: admin_params[:username].downcase)
    if admin
      if admin.authenticate(admin_params[:password])
        log_in_admin admin
        return redirect_to store_index_path, notice: 'Successfully logged in'
      end
    end
    flash[:danger] = 'Invalid username/password combination'
    render 'new'
  end

  def destroy
    session.delete :user_id
    redirect_to store_index_path, notice: 'Successfully logged out'
  end

  def admin_params
    params.require(:admin).permit(:username, :password)
  end
end
