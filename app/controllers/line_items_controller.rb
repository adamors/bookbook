class LineItemsController < ApplicationController
  include CartHelper
  before_action :set_cart

  def create
    book = Book.find params[:book_id]
    @line_item = @cart.add_book book
    if @line_item.save
      redirect_to cart_path(@cart), notice: 'Added book to cart'
    else
      redirect_to book_path(book), alert: 'Error adding book to cart. ' + @line_item.errors.full_messages.to_sentence
    end
  end
end
