<h6>Sapientia EMTE, Marosvásárhelyi Kar</h6>

<h4 style="font-family: sans-serif;font-weight: 100;">
  A Behavior Driven Development tanulmányozása webalkalmazások esetén
</h4>

<div>
  <div style="display: inline-block; float:left; text-align: left; font-size: 75%;">
  <br><br><br>Témavezető:<br>Dr. Antal Margit<br>egyetemi docens
  </div>
  <div style="display: inline-block; float:right; text-align: right; font-size: 75%;">
  <br><br><br>Végzős hallgató:<br>Kovács Örs
  </div>
  <div style="clear:both"></div>
  <h6>2017</h6>
</div>

---

###Dolgozat célja

- a Behavior Driven Development (BDD) módszer vizsgálata webalkalmazások esetén |
- webes alkalmazás fejlesztése BDD nélkül és BDD-vel |
- előnyök és hátrányok bemutatása |

---

###Szoftverek tesztelése

- ahogy fejlődött a szoftverfejlesztés, vele együtt fejlődtek a tesztelési technikák is |
- az első szoftvertesztelés könyv: G. J. Myers, The art of software testing, 1979 |
- ANSI/IEEE által közmegegyezéses szabványok |

+++

###Szofteverek tesztelése

- a dotkom-lufi ideje alatt nagyon megnő a termékek piacradobásának gyorsasága |
- felgyorsul a szoftverek követelményének a változása is |
- létező szoftverfejlesztési folyamatok nem tudnak egy elég rövid fejlesztési életciklust biztosítani |

+++

###Szofteverek tesztelése

- Chrysler Comprehensive Compensation System (C3) |
- eXtreme Programming |
- rövid feedback ciklus, fejlesztés előtti tesztek |

---

###Test-Driven Development

- nem a tesztelésről, hanem a szoftverfejlesztésről szól |
- egységtesztek mondják meg, azt, hogy a kód minek kell majd megfeleljen |
- rövid, könnyen megtanulható folyamat |

+++

###Test-Driven Development

![Picture1](slides/Picture1.png)

+++

###Test-Driven Development

- egyszerű lépések |
- "tudjuk mit kell leprogramozni" |
- kód szinten könnyebb OOP |
- mit teszteljünk ha még nincs egy sor kód se? |
- túl sok idő a tesztek írására |

---

###Behavior-Driven Development

- TDD oktatása során alakul ki |
- ne egy cselekményt ellenőrizzen, hanem egy viselkedést követeljen meg |
- egységtesztek emiatt inkább specifikációhoz kezdenek el hasonlítani |
- üzleti nyelv használata |

+++

###Behavior-Driven Development

```gherkin
Given a customer has a current account
When the customer transfers funds from this
  account to an overseas account
Then the funds should be deposited in
  the overseas account
And the transaction fee should be deducted
  from the current account
```

+++

###Behavior-Driven Development

![Picture2](slides/Picture2.png)

---

###Felhasznált technológiák

- Ruby programozási nyelv
- Ruby on Rails webes keretrendszer
- RSpec tesztelő rendszer
- Cucumber tesztelő rendszer

---

###Ruby

![Ruby](slides/ruby_logo.png)

- magas szintű, dinamikus és alapjaitól kezdve objektumorientált nyelv |
- szkriptnyelv |
- gyors fejlesztés |
- metaprogramozás |

+++

###Ruby

```ruby
myarray = [1, 2, [2], true, nil, 'My string']

myhash = { 'key' => 'value', :symbol_key => 'value', 2 => true }

if <feltétel>
  # igaz ág
else
  # különben ág
end
```

+++

###Ruby
```ruby
class Foo
  attr_writer :arg1

  def initialize(arg1)
    @arg1 = arg1
  end
end

f = Foo.new('arg')
puts f.arg1
f.arg1 = 'changed'

```

+++

###Ruby
```ruby
class Bird
  def fly
    puts 'bird flying'
  end
end
class Airplane
  def fly
    puts 'airplane flying'
  end
end
def lift_off(entity)
  entity.fly
end
lift_off(Bird.new)
lift_off(Airplane.new)

```
---

###Ruby on Rails

![Rails](slides/rails_logo.png)

- MVC webes keretrendszer |
- convention over configuration |
- gyors fejlesztés |
- batteries included |

+++

###Ruby on Rails

```ruby
#config/routes.rb
Rails.application.routes.draw do
  get 'posts' => 'post#index'
end

#app/controllers/post_controller.rb
class PostController < ApplicationController
  def index
    @posts = Post.all
  end
end
```

+++

###Ruby on Rails

```erb
#app/views/post/index.html.erb
<h1>Products</h1>
<table>
  <% @posts.each do |post| %>
  <tr>
    <td><%= post.name %></td>
    <td><%= post.title %></td>
    <td><%= post.content %></td>
  </tr>
  <% end %>
</table>
```

---

###RSpec

- tesztelő keretrendszer |
- szakterület-specifikus nyelv |
- könnyen olvasható és tanulható |

+++

###RSpec

```ruby
#bowling.rb
class Bowling
  attr_reader :score
  def initialize
    @score = 0
  end
  def hit(pin_count)
    @score += pin_count
  end
end
```

+++
###RSpec

```ruby
require 'bowling'
RSpec.describe Bowling, '#score' do

  subject { Bowling.new }

  context 'with no strikes or spares' do
    it 'sums the pin count for each roll' do
      20.times { subject.hit(4) }
      expect(subject.score).to eq(80)
    end
  end
end
```
---

###Alkalmazás

book-a-book egy online könyváruházt mintázó weboldal, ami regisztrált felhasználóknak lehetővé teszi, hogy könyveket vásároljanak.

![Picture3](slides/Picture3.png)

+++
####Alkalmazás
![usecase](slides/usecase.png)

+++

###Alkalmazás

![Picture4](slides/Picture4.png)

Kosár tartalmának listázása

+++

###Alkalmazás

![Picture10](slides/Picture10.png)

rendelést összegző oldal

+++

###Alkalmazás

![Picture5](slides/Picture5.png)

egyed-kapcsolati diagram

---

###Első fejlesztés

- "hagyományos" Rails alkalmazás |
- ActiveRecord |
- MVC |
- utólagos tesztelés |

+++

###Első fejlesztés

```ruby
def add_book
  book = Book.find params[:book_id]
  @line_item = @cart.add_book book
  if @line_item.save
    redirect_to view_cart_path, notice: 'Added book to cart'
  else
    redirect_to book_path(book), alert: "Error adding book to cart. #{@line_item.errors}"
  end
end
```

+++

###Első fejlesztés

```ruby
def save_address
  order_params = params.require(:order).permit(:name, :address, :country_id, :payment_type)
  @order = Order.find_by(id: session[:order_id])
  @order ||= Order.new
  @order.assign_attributes(order_params)
  unless @order.save
    redirect_to add_order_address_path, alert: "Error saving order. #{@order.errors.full_messages.join(', ')}"
    return
  end
end
```

+++

###Első fejlesztés

```ruby
def finish_checkout
  OrderMailer.new_order(@order).deliver
  OrderMailer.new_customer_order(@order).deliver
  @order.confirmed!
  @order.update_attribute(:user_id, @current_user.id)
  @order.update_book_sold_count
  session.delete :order_id
  session.delete :cart_id
  redirect_to root_path, notice: 'Order sent successfully'
end
```
---

###Első fejlesztés - tesztelés

```ruby
describe '#add_book' do
  let(:book) { FactoryGirl.create(:book) }
  let(:cart) { FactoryGirl.create(:cart) }

  context "when line item doesn't exist for book" do
    it 'creates a line item for book' do
      line_item = cart.add_book(book)
      expect(line_item).to be_instance_of(LineItem)
      expect(line_item.quantity).to eq(1)
      expect(line_item.book_id).to eq(book.id)
    end
  end
end
```

+++

###Első fejlesztés - tesztelés

```ruby
describe '#add_book' do
  let(:book) { FactoryGirl.create(:book) }
  let(:cart) { FactoryGirl.create(:cart) }

  subject { get(:add_book, params: { book_id: book.id }, session: { cart_id: cart.id }) }

  it 'adds the book to the cart' do
    expect(subject).to redirect_to view_cart_path
    expect(cart.line_items.count).to eq(1)
  end
end
```

+++

###Első fejlesztés - tesztelés

```ruby
context 'when address is corect' do
  it 'creates the order' do
    subject
    expect(Order.count).to eq(1)
  end

  it 'saves the order in the session' do
    subject
    expect(session[:order_id]).to_not be(nil)
  end
end
```
+++
###Első fejlesztés - tesztelés

```ruby
context 'when address is corect' do
  it "adds the cart's items to the order" do
    subject
    order = Order.find_by(id: session[:order_id])
    expect(order.line_items.count).to eq(cart.line_items.count)
  end

  it 'redirects to checkout' do
    expect(subject).to redirect_to orders_checkout_path
  end
end
```
---

###Második fejlesztés

- BDD |
- keretrendszert háttérbe szorító fejlesztés |
- Repository minta, memóriában tárolt adat |
- SRP objektumok |
- mocking |

+++

###Második fejlesztés

```gherkin
Feature: Store index
  In order to shop
  As a visitor
  I want to see new releases and bestsellers

Scenario: New releases list
    Given the store has new releases
    When I visit the index page
    Then I should see the new releases
```
+++
###Második fejlesztés
```ruby
RSpec.describe AddToCart do
  describe '#call' do
    let(:cart) do
      cart = instance_double('CartService')
      allow(cart).to receive(:add)
      cart
    end
    let(:params) { { book_id: 1 } }

    subject { AddToCart.call(cart: cart, params: params) }

    it 'adds a book to the cart' do
      cart.should receive(:add)
      subject
    end
  end
end
```

+++

###Második fejlesztés

```ruby
class AddToCart
  def call
    cart.add(book: book)
  end

  def self.call(cart:, params:)
    new(cart, params).call
  end

  private

  # ...

  def book
    BookRepository.find(params[:book_id])
  end
end
```

+++

###Második fejlesztés

```ruby
subject { CartService.new(session: session) }

context "when book isn't in the cart yet" do
  let(:book) { Book.new(id: 1) }
  let(:cart) { Cart.new(id: 1) }

  before do
    allow(CartRepository).to receive(:find).and_return(cart)
  end

  it 'creates a new line item' do
    subject.add(book: book)
    LineItemRepository.count.should == 1
  end
end
```

+++

###Második fejlesztés
```ruby
def add(book:)
  line_item = LineItemRepository.find_by(book_id: book.id, cart_id: cart.id)
  unless line_item
    return LineItemRepository.new_from(cart_id: cart.id, book: book)
  end
  line_item.quantity += 1
  LineItemRepository.save(line_item)
end
```

+++

###Második fejlesztés
```gherkin
Scenario: Login
    Given I am a visitor
    When I visit the index page
    Then I should see the login link
Scenario: My Account
    Given I am logged in
    When I visit the index page
    Then I should have access to the "Account" menu
```

+++

###Második fejlesztés
```ruby
#user_presenter
def initialize(session_service:)
  @session_service = session_service
end

def my_account(view_context:)
  if session_service.user_logged_in?
    view_context.render 'header_user_account'
  else
    view_context.render 'header_login'
  end
end
# ...
<%= user_presenter.my_account(view: self) %>
```
---

###Összefoglalás

- BDD gyorsabb tesztekhez vezet |
- mockolni tudunk sok mindent |
- könnyebb a TDD az elfogadási tesztek miatt |
- könnyen elhatárolható osztályok |
- Cucumber lassú és nehézkes |
- keretrendszerrel kell küzdeni |
