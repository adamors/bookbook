class AddTaxToOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :tax, :decimal, default: 0
  end
end
