class AddSoldCountToBooks < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :sold_count, :integer, default: 0
  end
end
