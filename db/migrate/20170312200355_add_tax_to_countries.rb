class AddTaxToCountries < ActiveRecord::Migration[5.0]
  def change
    add_column :countries, :tax, :decimal
  end
end
