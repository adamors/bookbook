class CreateLineItems < ActiveRecord::Migration[5.0]
  def change
    create_table :line_items do |t|
      t.integer  :book_id
      t.integer  :cart_id
      t.integer  :quantity, default: 1
      t.decimal  :price
      t.integer  :order_id

      t.timestamps
    end
  end
end
