require 'rails_helper'

RSpec.feature 'Store', type: :feature do
  feature 'index page' do
    let!(:new_books) { create_list(:book, 10) }
    let!(:best_sellers) { create_list(:book, 10, created_at: rand(2..10).days.ago, sold_count: rand(5..100)) }

    scenario 'viewing the store index' do
      visit root_path
      expect(page).to have_text('New releases')
      expect(page).to have_text('Bestsellers')

      new_releases_title = all(:css, '.new-releases .book-title').map(&:text)
      expect(new_releases_title).to match_array(new_books.pluck(:name))

      best_sellers_title = all(:css, '.best-sellers .book-title').map(&:text)
      expect(best_sellers_title).to match_array(best_sellers.pluck(:name))
    end
  end
end
