require 'rails_helper'

RSpec.describe AuthorsController, type: :controller do
  describe 'GET show' do
    let(:author) { create(:author) }

    it 'renders an author' do
      get :show, params: { id: author.id }
      expect(response).to have_http_status(200)
      expect(response).to render_template(:show)
    end
  end
end
