require 'rails_helper'

RSpec.describe OrdersController, type: :controller do
  describe '#save_address' do
    let(:user) { create(:user) }
    let(:cart) { create(:cart, :with_item) }
    let(:country) { create(:country) }
    let(:params) do
      {
        order: {
          name: 'Test',
          address: 'Street 12',
          country_id: country.id,
          payment_type: 'Pay on delivery'
        }
      }
    end

    subject do
      post :save_address, params: params, session: { user_id: user.id, cart_id: cart.id }
    end

    context 'when address is incorrect' do
      before { params[:order].delete(:country_id) }

      it 'redirect to address form' do
        expect(subject).to redirect_to add_order_address_path
      end

      it 'adds the errors to the flash' do
        subject
        expect(flash[:alert]).to match(/Error saving order.*/)
      end
    end

    context 'when address is corect' do
      it 'creates the order' do
        subject
        expect(Order.count).to eq(1)
      end

      it 'redirects to checkout' do
        expect(subject).to redirect_to orders_checkout_path
      end

      it 'saves the order in the session' do
        subject
        expect(session[:order_id]).to_not be(nil)
      end

      it "adds the cart's items to the order" do
        subject
        order = Order.find_by(id: session[:order_id])
        expect(order.line_items.count).to eq(cart.line_items.count)
      end
    end
  end

  describe '#checkout' do
    let(:user) { create(:user, email: 'asd@foo.com') }
    let(:cart) { create(:cart, :with_item) }

    context "when order doesn't exist" do
      subject do
        get :checkout, params: {}, session: { user_id: user.id, cart_id: cart.id }
      end

      it 'redirects to store path' do
        expect(subject).to redirect_to(store_index_path)
      end
    end

    context 'when order exists' do
      let(:country) { create(:country) }
      let(:order) { create(:order, country: country) }

      subject do
        get :checkout, params: {}, session: { user_id: user.id, cart_id: cart.id, order_id: order.id }
      end

      it 'calculates shipping cost' do
        expect { subject }.to change { order.reload.shipping_cost }
      end
    end
  end

  describe '#finish_checkout' do
    let(:user) { create(:user, email: 'asd@bar.com') }

    context "when order doesn't exist" do
      subject do
        post :finish_checkout, params: {}, session: { user_id: user.id }
      end

      it 'redirects to store path' do
        expect(subject).to redirect_to(store_index_path)
      end
    end

    context 'when order exists' do
      let(:country) { create(:country) }
      let(:order) { create(:order, country: country) }
      let(:cart) { create(:cart, :with_item) }

      subject do
        post :finish_checkout, params: {}, session: { user_id: user.id, order_id: order.id, cart_id: cart.id }
      end

      it 'sends an email to the admin and the user' do
        expect { subject }.to change { ActionMailer::Base.deliveries.count }.by(2)
      end

      it 'updates the order to confirmed' do
        expect { subject }.to change { order.reload.status }
      end

      it 'redirects to store index' do
        expect(subject).to redirect_to(root_path)
      end
    end
  end
end
