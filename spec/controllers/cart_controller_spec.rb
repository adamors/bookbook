require 'rails_helper'

RSpec.describe CartController, type: :controller do
  describe '#add_book' do
    let(:book) { create(:book) }
    let(:cart) { create(:cart) }

    subject do
      get :add_book, params: { book_id: book.id }, session: { cart_id: cart.id }
    end

    it 'adds the book to the cart' do
      expect(subject).to redirect_to view_cart_path
      expect(cart.line_items.count).to eq(1)
    end
  end
end
