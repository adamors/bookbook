require 'rails_helper'

RSpec.describe BooksController, type: :controller do
  describe 'GET show' do
    let(:book) { create(:book) }

    it 'renders a book' do
      get :show, params: { id: book.id }
      expect(response).to have_http_status(200)
      expect(response).to render_template(:show)
    end
  end
end
