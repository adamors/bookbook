require 'rails_helper'

RSpec.describe StoreController, type: :controller do
  describe 'GET index' do
    it 'renders a list of books' do
      get :index
      expect(response).to have_http_status(200)
      expect(response).to render_template(:index)
    end
  end
end
