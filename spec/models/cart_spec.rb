require 'rails_helper'

RSpec.describe Cart, type: :model do
  describe '#add_book' do
    let(:book) { create(:book) }
    let(:cart) { create(:cart) }

    context 'when line item exists for book' do
      let!(:line_item) { create(:line_item, book: book, cart: cart, quantity: 1) }

      it 'increments quantity of line item' do
        result_line_item = cart.add_book(book)
        expect(result_line_item.quantity).to eq(2)
      end
    end

    context "when line item doesn't exist for book" do
      it 'creates a line item for book' do
        line_item = cart.add_book(book)
        expect(line_item).to be_instance_of(LineItem)
        expect(line_item.quantity).to eq(1)
        expect(line_item.book_id).to eq(book.id)
      end
    end
  end
end
