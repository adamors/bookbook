FactoryGirl.define do
  factory :author do
    name 'John Smith'
  end

  factory :user do
    name 'Foo'
    email 'foo@bar.com'
    password 'password123'
  end

  factory :country do
    name 'Foo'
    tax 10
  end

  factory :category do
    name 'Book'
  end

  factory :book do
    sequence :name do |n|
      "Book#{n + 10}"
    end
    sequence :isbn do |n|
      n
    end
    cover ''
    price { rand(1..100) }
    sold_count 0
    created_at { DateTime.now }
    association :category
    association :author
  end

  factory :cart do
    trait :with_item do
      after(:create) do |cart|
        category = create(:category)
        author = create(:author)
        book = create(:book, category: category, author: author)
        create(:line_item, book: book, cart: cart, quantity: 1)
      end
    end
  end

  factory :line_item do
    association :book
    association :cart
    quantity 0
    price { rand(1..100) }
  end

  factory :order do
    name 'Order'
    address 'Street'
    association :country
    payment_type 1
    status 0
    shipping_cost 0
    association :user
    tax 0
  end
end
